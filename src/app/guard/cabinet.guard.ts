import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';

import { PrincipalService } from '../modules/core/services/principal.service';
import { getCabinetLink } from '../modules/core/models/principal.model';

@Injectable({
  providedIn: 'root'
})
export class CabinetGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private principalService: PrincipalService,
              private router: Router) {}

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canVisit(route);
  }
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(childRoute, state);
  }
  canLoad(route: Route, segments: UrlSegment[]):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  private canVisit(route: ActivatedRouteSnapshot): boolean {
    if (!route.data.role) {
      return this.canVisitLanding(route);
    } else {
      return this.canVisitCabinet(route);
    }
  }

  private canVisitLanding(route: ActivatedRouteSnapshot): boolean {
    const role = this.principalService.getUserInfoSnapshot().role;
    if (role) {
      this.router.navigate([getCabinetLink(role)]);
      return false;
    }
    return true;
  }

  private canVisitCabinet(route: ActivatedRouteSnapshot): boolean {
    const canVisit = route.data.role === this.principalService.getUserInfoSnapshot().role;
    if (!canVisit) {
      this.principalService.logout();
    }
    return canVisit;
  }
}
