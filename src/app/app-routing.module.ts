import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CabinetGuard } from './guard/cabinet.guard';
import { AccountRole } from './modules/core/models/account.models';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/landing/landing.module').then(m => m.LandingModule),
    canActivate: [CabinetGuard]
  },
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    canActivate: [CabinetGuard]
  },
  {
    path: 'admin',
    loadChildren: () => import('./modules/cabinets/admin/admin.module').then(m => m.AdminModule),
    canActivate: [CabinetGuard],
    data: {
      role: AccountRole.ROLE_ADMIN
    }
  },
  {
    path: 'client',
    loadChildren: () => import('./modules/cabinets/client/client.module').then(m => m.ClientModule),
    canActivate: [CabinetGuard],
    data: {
      role: AccountRole.ROLE_CLIENT
    }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
