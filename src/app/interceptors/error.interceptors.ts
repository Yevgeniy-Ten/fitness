import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PrincipalService } from '@core/services/principal.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private principalService: PrincipalService,
        private router: Router,
        private toastr: ToastrService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((err) => {
                const message = err?.error?.message || 'Something went wrong';
                const title = err?.error?.type || 'Error';
                this.toastr.error(message, title);
                if (err.status === 401) {
                    this.logout();
                } else if (err.status === 403) {
                    this.forbidVisitingPage();
                }
                return throwError(err);
            })
        );
    }

    private forbidVisitingPage(): void {
        this.toastr.warning('У вас недостаточно прав для просмотра данной страницы');
        this.principalService.logout();
    }

    private logout(): void {
        this.toastr.warning('Валидность токена истекла');
        this.principalService.logout();
    }
}
