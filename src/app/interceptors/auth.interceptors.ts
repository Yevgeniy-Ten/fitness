import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageKey } from '@core/models/localstorage.model';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem(LocalStorageKey.ACCESS_TOKEN);
        if (!token) {
          return next.handle(request);
        }
        const req = request.clone({
            headers: request.headers.append(
                'Authorization', `Bearer ${token}`,
            )
        });
        return next.handle(req);
    }
}
