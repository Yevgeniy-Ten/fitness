import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from '@modules/landing/landing.component';
import { BannerPageComponent } from '@modules/landing/banner-page/banner-page.component';
import { NavigationPageComponent } from '@modules/landing/navigation-page/navigation-page.component';
import { PageNotFoundComponent } from '@core/ui/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      { path: '', component: BannerPageComponent, data: { animation: 'BannerPage' } },
      { path: 'navigation', component: NavigationPageComponent, data: { animation: 'NavigationPage' } },
      { path: 'price-list', component: PageNotFoundComponent },
      { path: 'schedule', component: PageNotFoundComponent },
      { path: 'about-us', component: PageNotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
