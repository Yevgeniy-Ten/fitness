import { Component } from '@angular/core';

@Component({
  selector: 'app-banner-page',
  templateUrl: './banner-page.component.html',
  styleUrls: ['./banner-page.component.sass']
})

export class BannerPageComponent {

  consultantPhoneNumber = '77012666292';

  navigateToWhatsApp(): void {
    window.open(`https://wa.me/${this.consultantPhoneNumber}`, '_blank', 'noopener, noreferrer');
  }
}
