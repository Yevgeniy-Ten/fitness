import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { BannerPageComponent } from './banner-page/banner-page.component';
import { NavigationPageComponent } from './navigation-page/navigation-page.component';
import { LandingComponent } from './landing.component';
import { HeaderComponent } from './header/header.component';
import { SocialNetworksComponent } from './social-networks/social-networks.component';
import { NavSectionComponent } from './nav-section/nav-section.component';
import { TranslateModule } from '@ngx-translate/core';
import { CoreModule } from '@core/core.module';


@NgModule({
  declarations: [
    BannerPageComponent,
    NavigationPageComponent,
    LandingComponent,
    HeaderComponent,
    SocialNetworksComponent,
    NavSectionComponent
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    TranslateModule,
    CoreModule
  ]
})
export class LandingModule { }
