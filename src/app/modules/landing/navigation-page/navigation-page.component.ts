import { Component } from '@angular/core';
import { NavSectionNames } from '@core/models/navigation.model';

@Component({
  selector: 'app-navigation-page',
  templateUrl: './navigation-page.component.html',
  styleUrls: ['./navigation-page.component.sass']
})
export class NavigationPageComponent {

  sectionNames = NavSectionNames;
  activeSection = NavSectionNames.AQUA;

  changeActiveSection(sectionName: NavSectionNames): void {
    this.activeSection = sectionName;
  }

  isSectionActive(sectionName: NavSectionNames): boolean {
    return this.activeSection === sectionName;
  }

}
