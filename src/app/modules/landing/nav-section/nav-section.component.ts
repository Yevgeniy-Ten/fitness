import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NavSectionNames } from '@core/models/navigation.model';
import { NavigationService } from '@core/services/navigation.service';

@Component({
  selector: 'app-nav-section',
  templateUrl: './nav-section.component.html',
  styleUrls: ['./nav-section.component.sass']
})
export class NavSectionComponent {

  @Input() sectionName!: NavSectionNames;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onHover = new EventEmitter<NavSectionNames>();

  constructor(private navigationService: NavigationService) {}

  getImg(): string {
    return this.navigationService.getNavSectionBackgroundImg(this.sectionName);
  }

  changeState(): void {
    this.onHover.emit(this.sectionName);
  }

}
