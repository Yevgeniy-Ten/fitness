import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { landingRouteAnimation } from '@core/utils/route-animations';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.sass'],
  animations: [landingRouteAnimation]
})
export class LandingComponent {

  prepareOutlet(outlet: RouterOutlet): string {
    return outlet?.activatedRouteData?.animation;
  }

}
