import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonComponent } from './ui/button/button.component';
import { RhombusesComponent } from './ui/spinners/rhombuses/rhombuses.component';
import { CabinetHeaderComponent } from './ui/cabinet-header/cabinet-header.component';
import { NavMenuComponent } from '@core/ui/nav-menu/nav-menu.component';
import { ContainerPageComponent } from '@core/ui/container-page/container-page.component';
import { HamburgerButtonComponent } from './ui/hamburger-button/hamburger-button.component';
import { ImgComponent } from '@core/ui/img/img.component';
import { TranslateModule } from '@ngx-translate/core';
import { LangSelectComponent } from './ui/lang-select/lang-select.component';
import { FormsModule } from '@angular/forms';
import { DateLocaleFilterPipe } from './pipes/date-locale-filter.pipe';
import { PageNotFoundComponent } from './ui/page-not-found/page-not-found.component';
import { PaginationComponent } from './ui/pagination/pagination.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  declarations: [
    ButtonComponent,
    RhombusesComponent,
    CabinetHeaderComponent,
    NavMenuComponent,
    ContainerPageComponent,
    HamburgerButtonComponent,
    ImgComponent,
    LangSelectComponent,
    DateLocaleFilterPipe,
    PageNotFoundComponent,
    PaginationComponent
  ],
  exports: [
    ButtonComponent,
    CabinetHeaderComponent,
    NavMenuComponent,
    ContainerPageComponent,
    ImgComponent,
    LangSelectComponent,
    DateLocaleFilterPipe,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    PaginationModule
  ]
})
export class CoreModule { }
