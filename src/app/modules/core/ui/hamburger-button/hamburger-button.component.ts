import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hamburger-button',
  templateUrl: './hamburger-button.component.html',
  styleUrls: ['./hamburger-button.component.sass']
})
export class HamburgerButtonComponent implements OnInit {

  @Input() open = false;

  constructor() { }

  ngOnInit(): void {
  }

}
