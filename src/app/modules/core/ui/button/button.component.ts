import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.sass']
})
export class ButtonComponent implements OnInit {

  @Input() label!: string;
  @Input() color!: string;
  @Input() disabled!: boolean;
  @Input() loading!: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
