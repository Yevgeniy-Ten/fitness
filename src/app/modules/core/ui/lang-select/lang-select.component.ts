import { Component } from '@angular/core';
import { Languages } from '@core/models/lang.model';
import { LangService } from '@core/services/lang.service';

@Component({
  selector: 'app-lang-select',
  templateUrl: './lang-select.component.html',
  styleUrls: ['./lang-select.component.sass']
})
export class LangSelectComponent {

  languages = Languages;

  constructor(private langService: LangService) { }

  isChecked(lang: Languages): boolean {
    let selectedLang = null;
    this.langService.selected$.subscribe(l => selectedLang = l);
    return selectedLang === lang;
  }

  selectLanguage(lang: Languages): void {
    this.langService.selectLanguage(lang);
  }
}
