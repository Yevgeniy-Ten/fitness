import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-container-page',
  templateUrl: './container-page.component.html',
  styleUrls: ['./container-page.component.sass']
})
export class ContainerPageComponent {

  @Input() loading = false;
  @Input() title!: string;

}
