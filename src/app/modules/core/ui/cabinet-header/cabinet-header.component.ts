import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cabinet-header',
  templateUrl: './cabinet-header.component.html',
  styleUrls: ['./cabinet-header.component.sass']
})
export class CabinetHeaderComponent {

  @Input() title!: string;

}
