import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.sass']
})
export class PaginationComponent {

  @Output() pageChanged = new EventEmitter<number>();
  @Input() totalItems!: number;
  @Input() maxSize!: number;

}
