import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {environment} from '@env/environment';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImgComponent {

  style = new BehaviorSubject<any>({ backgroundImage: 'url("~src/assets/icons/accounts.png")' });

  @Input()
  set imageUuid(imageUuid: string | undefined) {
    if (!imageUuid) {
      return;
    }
    this.style.next({ backgroundImage: `url(${environment.api}/files/${imageUuid})` });
  }

}
