import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { PrincipalService } from '@core/services/principal.service';
import { LangService } from '@core/services/lang.service';
import { Languages } from '@core/models/lang.model';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideInOut', [
    state('false', style({
      width: '4.2rem'
    })),
    state('true', style({
      width: '19rem'
    })),
    transition('false <=> true', animate('200ms ease-in-out'))
    ])
  ]
})
export class NavMenuComponent implements OnInit {

  @Input() role!: string;
  @Input() routes!: { url: string, name: string, icon: string }[];
  currentRoute!: string;
  isMenuOpen = false;
  languages = Languages;

  constructor(private principalService: PrincipalService,
              private router: Router,
              public langService: LangService) { }

  ngOnInit(): void {
    this.getCurrentRoute();
  }

  logout(): void {
    this.principalService.logout();
  }

  goTo(url: string): void {
    this.router.navigate([`${url}`]);
  }

  private getCurrentRoute(): void {
    this.currentRoute = this.router.url;
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => this.currentRoute = (event as NavigationEnd).url );
  }

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }
}
