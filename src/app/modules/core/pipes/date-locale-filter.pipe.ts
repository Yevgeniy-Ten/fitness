import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { LangService } from '@core/services/lang.service';

@Pipe({
  name: 'dateLocaleFilter'
})
export class DateLocaleFilterPipe implements PipeTransform {

  constructor(private langService: LangService) {}

  transform(date: moment.Moment | string, dateFormat = 'DD MMMM yyyy'): Observable<string> {
    return this.langService.selected$
      .pipe(
        map(lang => moment(date).locale(moment.locale(lang)).format(dateFormat))
      );
  }

}
