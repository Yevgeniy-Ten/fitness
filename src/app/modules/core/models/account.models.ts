export enum AccountRole {
  ROLE_ADMIN = 'ROLE_ADMIN',
  ROLE_CLIENT = 'ROLE_CLIENT'
}

export enum AccountStatus {
  PENDING = 'PENDING',
  ACTIVE = 'ACTIVE',
  DEACTIVATED = 'DEACTIVATED'
}

export function getAccountRoleNameI18nKey(role: AccountRole): string {
  switch (role) {
    case AccountRole.ROLE_ADMIN: return 'roles.admin';
    case AccountRole.ROLE_CLIENT: return 'roles.client';
  }
}

export interface AccountDTO {
  id: number;
  username: string;
  fullname: string;
  email: string;
  phone: string;
  role: AccountRole;
  status: AccountStatus;
}
