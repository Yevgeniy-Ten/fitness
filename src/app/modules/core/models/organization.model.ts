export interface OrganizationDTO {
  id: string;
  name: string;
  address: string;
  openingTime: string;
  closingTime: string;
}
