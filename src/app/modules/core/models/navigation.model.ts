export enum NavSectionNames {
  AQUA = 'AQUA',
  BEAUTY = 'BEAUTY',
  CLINIC = 'CLINIC',
  FITNESS = 'FITNESS',
  MED_DETOX = 'MED-DETOX',
  SPA = 'SPA'
}
