import {OfferedServiceType} from '@core/models/offered-service.model';

export enum AppointmentStatus {
  PENDING = 'PENDING',
  DECLINED = 'DECLINED',
}

export interface CreateAppointmentRequest {
  serviceId: number;
  appointmentDateTime: string;
  clientId: number;
  appointmentRequestId: number;
}

export interface AppointmentDTO {
  date: string;
  id: number;
  service: string;
  time: string;
  type: OfferedServiceType;
  status: AppointmentStatus;
  address: string;
}

export interface AppointmentRequestDTO {
  comment: string;
  organizationId: string;
  offeredServiceId: string;
  employeeName: string;
  employeeId: string;
  hh: number;
  mm: number;
  date: string;
}

export const APPOINTMENT_DATE_FORMAT = 'DD-MM-YYYY';
