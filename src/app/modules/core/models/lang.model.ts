export enum Languages {
  KAZAKH = 'kk',
  RUSSIAN = 'ru',
  ENGLISH = 'en'
}
