export enum LocalStorageKey {
  ACCESS_TOKEN = 'grandprix_access_token',
  USERNAME = 'grandprix_username',
  ROLE = 'grandprix_role',
  LANGUAGE = 'grandprix_language'
}
