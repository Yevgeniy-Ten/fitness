import { AccountRole } from './account.models';

export interface IAuthRequest {
  username: string;
  password: string;
  role: AccountRole;
  organization?: string;
}

export interface IAuthResult {
  username: string;
  role: AccountRole;
  token: string;
}
