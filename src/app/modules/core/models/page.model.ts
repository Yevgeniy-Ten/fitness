export interface Page<T> {
  totalElements: number;
  empty: boolean;
  content: T[];
}
