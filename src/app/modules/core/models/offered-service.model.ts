export enum OfferedServiceType {
  BEAUTY = 'BEAUTY',
  FITNESS = 'FITNESS',
  MED = 'MED',
}

export interface OfferedServiceDTO {
  id: string;
  nameRu: string;
  nameKz: string;
  type: string;
}
