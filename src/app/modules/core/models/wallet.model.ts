export interface PaymentPayloadDTO {
  publicId: string;
  description: string;
  amount: number;
  currency: string;
  accountId?: number;
  email?: string;
  skin?: string;
  data?: {
    phone?: string;
    name?: string;
    nick?: string;
  };
}

export interface TopUpDTO {
  transactionId: number;
  amount: number;
  currency: string;
  accountId: string;
  email: string;
  details: string;
  paymentDate: string;
  status: TopUpStatus;
  reason: string;
  cardHolderMessage: string;
}

export enum TopUpStatus {
  DECLINED = 'Declined',
  COMPLETED = 'Completed'
}
