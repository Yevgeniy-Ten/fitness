export interface File64DTO {
  uuid: string;
  filename: string;
  content: string;
}
