import { AccountRole } from './account.models';

export function getCabinetLink(role: AccountRole): string {
  switch (role) {
    case AccountRole.ROLE_ADMIN: return 'admin';
    case AccountRole.ROLE_CLIENT: return 'client';
  }
}

export interface CurrentUser {
  accessToken: string | null;
  username: string | null;
  role: AccountRole | null;
}
