import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { IAuthRequest, IAuthResult } from '../models/auth.models';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(private http: HttpClient) { }

  signin(payload: IAuthRequest): Observable<IAuthResult> {
    return this.http.post<IAuthResult>(`${environment.auth}/signin`, payload);
  }

  signup(payload: IAuthRequest): Observable<any> {
    return this.http.post<any>(`${environment.api}/accounts`, payload);
  }
}
