import { Injectable } from '@angular/core';
import { AccountService } from '@core/services/account.service';
import { environment } from '@env/environment';
import { AccountDTO } from '@core/models/account.models';
import { PaymentPayloadDTO, TopUpDTO } from '@core/models/wallet.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { Page } from '@core/models/page.model';

@Injectable({
  providedIn: 'root'
})

export class WalletService {

  private CLOUD_PAYMENTS_WIDGET_LINK = 'https://widget.cloudpayments.ru/bundles/cloudpayments';

  constructor(private accountService: AccountService,
              private http: HttpClient) { }

  loadWidgetScript(): void {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = this.CLOUD_PAYMENTS_WIDGET_LINK;
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  loadWidget(): any {
    // @ts-ignore
    const widget = new cp.CloudPayments({language: 'ru-RU'});
    return widget;
  }

  generatePaymentPayload(accountInfo: AccountDTO, paymentAmount: number): PaymentPayloadDTO {
    return {
      publicId: environment.cloudPaymentsPublicId,
      description: environment.cloudPaymentsDescription,
      amount: paymentAmount,
      currency: 'KZT',
      accountId: accountInfo.id,
      email: accountInfo.email,
      skin: 'modern',
      data: {
        phone: accountInfo.phone,
        name: accountInfo.fullname,
        nick: accountInfo.username
      }
    };
  }

  saveTopUp(): Observable<any> {
    const date = moment().format('YYYY-MM-DD');
    return this.http.post(`${environment.api}/wallet`, date);
  }

  getAccountBalance(): Observable<number> {
    return this.http.get<number>(`${environment.api}/wallet/balance`);
  }

  getTopUps(pageNumber = 0, pageSize = 10): Observable<Page<TopUpDTO>> {
    return this.http.get<Page<TopUpDTO>>(`${environment.api}/wallet/topups?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
}
