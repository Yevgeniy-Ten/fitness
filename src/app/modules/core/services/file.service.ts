import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from '@env/environment';
import { File64DTO } from '@core/models/file.models';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  upload(file: File): Observable<string> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post(`${environment.api}/files`, formData, { responseType: 'text' });
  }

  getBase64(uuid: string): Observable<File64DTO> {
    return this.http.get<File64DTO>(`${environment.api}/files/${uuid}/base64`);
  }
}
