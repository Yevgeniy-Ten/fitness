import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { AccountDTO, AccountStatus } from '@core/models/account.models';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getAccounts(): Observable<AccountDTO[]> {
    return this.http.get<AccountDTO[]>(`${environment.api}/accounts`);
  }

  updateAccountStatus(id: number, status: AccountStatus): Observable<AccountStatus> {
    return this.http.put<AccountStatus>(`${environment.api}/accounts/${id}/status`, status);
  }

  getAccountInfo(): Observable<AccountDTO> {
    return this.http.get<AccountDTO>(`${environment.api}/accounts/info`);
  }
}
