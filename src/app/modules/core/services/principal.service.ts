import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { IAuthResult } from '../models/auth.models';
import { CurrentUser } from '../models/principal.model';
import { LocalStorageKey } from '../models/localstorage.model';
import { AccountRole } from '../models/account.models';

function getUserInfoFromLocalStorage(): CurrentUser {
  return {
    accessToken: localStorage.getItem(LocalStorageKey.ACCESS_TOKEN),
    username: localStorage.getItem(LocalStorageKey.USERNAME),
    role: localStorage.getItem(LocalStorageKey.ROLE) as AccountRole
  };
}

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  user$ = new BehaviorSubject<CurrentUser>(getUserInfoFromLocalStorage());

  constructor(private router: Router) { }

  private next(): void {
    this.user$.next(getUserInfoFromLocalStorage());
  }

  setUser(user: IAuthResult): void {
    localStorage.setItem(LocalStorageKey.ACCESS_TOKEN, user.token);
    localStorage.setItem(LocalStorageKey.USERNAME, user.username);
    localStorage.setItem(LocalStorageKey.ROLE, user.role);
    this.next();
  }

  getUserInfoSnapshot(): CurrentUser {
    return getUserInfoFromLocalStorage();
  }

  isAuthenticated(): boolean {
    const accessToken = localStorage.getItem(LocalStorageKey.ACCESS_TOKEN);
    return !!accessToken;
  }

  logout(): void {
    localStorage.removeItem(LocalStorageKey.ACCESS_TOKEN);
    localStorage.removeItem(LocalStorageKey.USERNAME);
    localStorage.removeItem(LocalStorageKey.ROLE);
    this.next();
    this.router.navigate(['']);
  }
}
