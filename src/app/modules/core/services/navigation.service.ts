import { Injectable } from '@angular/core';

import { NavSectionNames } from '@core/models/navigation.model';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  getNavSectionBackgroundImg(sectionName: NavSectionNames): string {
    return `assets/pics/${sectionName?.toLowerCase()}.png`;
  }

}
