import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { Languages } from '@core/models/lang.model';
import { LocalStorageKey } from '@core/models/localstorage.model';

@Injectable({
  providedIn: 'root'
})
export class LangService {
  lang$!: BehaviorSubject<Languages>;

  get selected$(): Observable<Languages> {
    return this.lang$.asObservable();
  }

  constructor(private translateService: TranslateService) {
    translateService.setDefaultLang(Languages.RUSSIAN);

    const lang = (localStorage.getItem(LocalStorageKey.LANGUAGE) as Languages) || Languages.RUSSIAN;
    this.lang$ = new BehaviorSubject<Languages>(lang);
    this.selectLanguage(lang);
  }

  selectLanguage(lang: Languages): void {
    localStorage.setItem(LocalStorageKey.LANGUAGE, lang);
    this.translateService.use(lang);
    this.lang$.next(lang);
  }
}
