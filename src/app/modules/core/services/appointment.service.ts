import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppointmentDTO } from '@core/models/appointment.model';
import { environment } from '@env/environment';
import { AppointmentRequestDTO } from '@core/models/appointment.model';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient) { }

  getAppointments(fromDate: string, tillDate: string): Observable<AppointmentDTO[]> {
    return this.http.get<AppointmentDTO[]>(`${environment.api}/appointments?fromDate=${fromDate}&tillDate=${tillDate}`);
  }

  createAppointmentRequest(request: AppointmentRequestDTO): Observable<number> {
    return this.http.post<number>(`${environment.api}/appointments`, request);
  }

}
