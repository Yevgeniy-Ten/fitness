import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { OrganizationDTO } from '../models/organization.model';

@Injectable({
  providedIn: 'root'
})

export class OrganizationService {

  constructor(private http: HttpClient) { }

  getOrganizations(): Observable<OrganizationDTO[]> {
    return this.http.get<OrganizationDTO[]>(`${environment.api}/organizations`);
  }

}
