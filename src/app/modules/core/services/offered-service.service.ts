import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { OfferedServiceDTO } from '@core/models/offered-service.model';

@Injectable({
  providedIn: 'root'
})
export class OfferedServiceService {

  constructor(private http: HttpClient) { }

  getOfferedServices(): Observable<OfferedServiceDTO[]> {
    return this.http.get<OfferedServiceDTO[]>(`${environment.api}/offered-services`);
  }

}
