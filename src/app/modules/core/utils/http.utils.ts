import {HttpParams} from '@angular/common/http';

export function createHttpParams(args: any): HttpParams {
  return Object.keys(args)
    .filter(key => args[key] !== undefined)
    .filter(key => args[key] !== null)
    .reduce((accumulator, current) => {
      return accumulator.append(current, args[current]);
    }, new HttpParams());
}
