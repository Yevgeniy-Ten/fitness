import { animate, group, query, style, transition, trigger } from '@angular/animations';

export const landingRouteAnimation =
trigger('slideInLeft', [
  transition('BannerPage <=> NavigationPage', [
    query(':enter, :leave', [
      style({
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        width: '100%'
      })
    ]),
    query(':enter', [style({ right: '-100%' })]),
    group([
      query(':leave', [animate('0.4s ease-out', style({ right: '100%' }))]),
      query(':enter', [animate('0.4s ease-out', style({ right: '0%' }))])
    ]),
  ])
]);
