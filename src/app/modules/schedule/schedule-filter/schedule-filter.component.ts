import {Component, Output, EventEmitter} from '@angular/core';
import {OrganizationDTO} from '@core/models/organization.model';
import {OrganizationService} from '@core/services/organization.service';
import {Observable} from 'rxjs';
import {ScheduleFilterParams} from '@modules/schedule/model/schedule-filter-params';
import {OfferedServiceDTO} from '@core/models/offered-service.model';
import {OfferedServiceService} from '@core/services/offered-service.service';

@Component({
  selector: 'app-schedule-filter',
  templateUrl: './schedule-filter.component.html',
  styleUrls: ['./schedule-filter.component.sass']
})
export class ScheduleFilterComponent {

  @Output() filterDataChanged = new EventEmitter<ScheduleFilterParams>();

  filterData: ScheduleFilterParams = {};

  offeredServices$: Observable<OfferedServiceDTO[]>;
  organizations$: Observable<OrganizationDTO[]>;

  constructor(private offeredServiceService: OfferedServiceService,
              private organizationService: OrganizationService) {
    this.offeredServices$ = this.offeredServiceService.getOfferedServices();
    this.organizations$ = this.organizationService.getOrganizations();
  }

  onSelectedDateChanged($event: string): void {
    this.filterData.selectedDate = $event;
    this.emit();
  }

  emit(): void {
    this.filterDataChanged.emit(this.filterData);
  }
}
