import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ScheduleFilterParams } from '@modules/schedule/model/schedule-filter-params';
import { createHttpParams } from '@core/utils/http.utils';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { DaySchedule } from '@modules/schedule/model/day-schedule';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  getSchedule(filterParams: ScheduleFilterParams): Observable<DaySchedule> {
    const params = createHttpParams(filterParams);
    console.log(params);
    return this.http.get<DaySchedule>(`${environment.api}/schedule`, { params });
  }
}
