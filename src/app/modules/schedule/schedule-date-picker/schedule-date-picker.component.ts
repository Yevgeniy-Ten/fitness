import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-schedule-date-picker',
  templateUrl: './schedule-date-picker.component.html',
  styleUrls: ['./schedule-date-picker.component.sass']
})
export class ScheduleDatePickerComponent implements OnInit {

  @Output() selectedDateChanged = new EventEmitter<string>();
  selectedDate!: moment.Moment;

  ngOnInit(): void {
    this.selectedDate = moment(new Date());
    this.emit();
  }

  prev(): void {
    this.selectedDate = moment(this.selectedDate).subtract(1, 'day');
    this.emit();
  }

  next(): void {
    this.selectedDate = moment(this.selectedDate).add(1, 'day');
    this.emit();
  }

  private emit(): void {
    this.selectedDateChanged.emit(this.selectedDate.format('DD-MM-YYYY'));
  }
}
