export interface SelectedTimeslot {
  timeslot: number;
  employee: EmployeeSchedule;
}

export interface ScheduleTimeFrame {
  startHour: number;
  startMinute: number;
  endHour: number;
  endMinute: number;
}

export interface EmployeeSchedule {
  ID: string;
  fullname: string;
  organizationName: string;
  organizationId: string;
  imageUuid: string;
  reserved: ScheduleTimeFrame[];
  workingHours: ScheduleTimeFrame[];
}

export interface DaySchedule {
  day: string;
  workingHours: ScheduleTimeFrame;
  employees: EmployeeSchedule[];
}
