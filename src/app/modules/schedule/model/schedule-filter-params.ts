export interface ScheduleFilterParams {
  organizationId?: string;
  offeredServiceId?: string;
  selectedDate?: string;
}
