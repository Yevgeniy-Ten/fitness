import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleDatePickerComponent } from './schedule-date-picker/schedule-date-picker.component';
import { ScheduleFilterComponent } from './schedule-filter/schedule-filter.component';
import { FormsModule } from '@angular/forms';
import { ScheduleInfoComponent } from './schedule-info/schedule-info.component';
import { EmployeeScheduleInfoComponent } from './employee-schedule-info/employee-schedule-info.component';
import {CoreModule} from "@core/core.module";
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    ScheduleDatePickerComponent,
    ScheduleFilterComponent,
    ScheduleInfoComponent,
    EmployeeScheduleInfoComponent
  ],
  exports: [
    ScheduleFilterComponent,
    ScheduleInfoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    TranslateModule
  ]
})
export class ScheduleModule { }
