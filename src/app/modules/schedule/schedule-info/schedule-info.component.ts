import {Component, Input, Output, EventEmitter} from '@angular/core';
import {DaySchedule, EmployeeSchedule, SelectedTimeslot} from '@modules/schedule/model/day-schedule';

@Component({
  selector: 'app-schedule-info',
  templateUrl: './schedule-info.component.html',
  styleUrls: ['./schedule-info.component.sass']
})
export class ScheduleInfoComponent {

  @Input() info!: DaySchedule;
  @Output() slotClicked = new EventEmitter<SelectedTimeslot>();

  get headerHeight(): string {
    return '150px';
  }

}
