import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {EmployeeSchedule, ScheduleTimeFrame, SelectedTimeslot} from '@modules/schedule/model/day-schedule';
import {ToastrService} from 'ngx-toastr';

const MINUTES_IN_HOUR = 60;
const PIXELS_HEIGHT_PER_MINUTE = 3;

@Component({
  selector: 'app-employee-schedule-info',
  templateUrl: './employee-schedule-info.component.html',
  styleUrls: ['./employee-schedule-info.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeScheduleInfoComponent {

  @Input() employee!: EmployeeSchedule;
  @Input() workingHours!: ScheduleTimeFrame;
  @Input() headerHeight!: string;
  @Input() pixelsPerMinute: number = PIXELS_HEIGHT_PER_MINUTE;
  @Output() slotClicked = new EventEmitter<SelectedTimeslot>();

  constructor(private toastr: ToastrService) {}

  get cadence(): number {
    return 15;
  }

  get slotHeight(): string {
    return `${(this.cadence * this.pixelsPerMinute)}px`;
  }

  get timeslots(): number[] {
    const { startHour, startMinute, endHour, endMinute } = this.workingHours;

    let start = this.toMinutes(startHour, startMinute) - startMinute % this.cadence;
    const end = this.toMinutes(endHour, endMinute);
    const arr: number[] = [];
    while (start < end) {
      arr.push(start);
      start += this.cadence;
    }
    return arr;
  }

  toMinutes(hh: number, mm: number): number {
    return hh * MINUTES_IN_HOUR + mm;
  }

  isReserved(ts: number): boolean {
    return this.employee.reserved.some(
      rts => this.toMinutes(rts.startHour, rts.startMinute) <= ts && this.toMinutes(rts.endHour, rts.endMinute) > ts
    );
  }

  isNotWorkingHours(ts: number): boolean {
    const startWorkingAt = this.toMinutes(this.employee.workingHours[0].startHour, this.employee.workingHours[0].startMinute);
    const endWorkingAt = this.toMinutes(this.employee.workingHours[0].endHour, this.employee.workingHours[0].endMinute);
    return ts < startWorkingAt || ts > endWorkingAt;
  }

  selectSlot(ts: number): void {
    if (this.isReserved(ts)) {
      return;
    }
    this.slotClicked.emit({ timeslot: ts, employee: this.employee });
  }

  formatTimeslot(ts: number): string {
    const format = (n: number) => (n < 10 ? '0' : '') + n;
    const hh = Math.floor(ts / MINUTES_IN_HOUR);
    const mm = ts % MINUTES_IN_HOUR;
    return `${format(hh)}:${format(mm)}`;
  }
}
