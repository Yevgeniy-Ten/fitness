import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from '@modules/cabinets/admin/admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminAccountsContainerComponent } from './admin-accounts-container/admin-accounts-container.component';
import { AdminAccountsTableComponent } from './admin-accounts-table/admin-accounts-table.component';
import { CoreModule } from '@core/core.module';
import { FormsModule } from '@angular/forms';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TopupsContainerComponent } from './topups-container/topups-container.component';
import { TopupsComponent } from './topups/topups.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminAccountsContainerComponent,
    AdminAccountsTableComponent,
    TopupsContainerComponent,
    TopupsComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CoreModule,
    FormsModule,
    NgxMaterialTimepickerModule
  ]
})
export class AdminModule { }
