import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from '@modules/cabinets/admin/admin.component';
import {AdminAccountsContainerComponent} from '@modules/cabinets/admin/admin-accounts-container/admin-accounts-container.component';
import { TopupsContainerComponent } from '@modules/cabinets/admin/topups-container/topups-container.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'accounts', component: AdminAccountsContainerComponent },
      { path: 'topups', component: TopupsContainerComponent },
      { path: '**', redirectTo: 'accounts' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
