import {Component} from '@angular/core';
import {AccountRole, getAccountRoleNameI18nKey} from '@core/models/account.models';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent {

  accountRoleName = getAccountRoleNameI18nKey(AccountRole.ROLE_ADMIN);

  routes = [
    { url: '/admin/accounts', name: 'аккаунты', icon: 'accounts' },
    { url: '/admin/topups', name: 'пополнения', icon: 'wallet'}
  ];

}
