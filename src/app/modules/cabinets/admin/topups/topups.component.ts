import { Component, Input, OnInit } from '@angular/core';
import { TopUpDTO, TopUpStatus } from '@core/models/wallet.model';

@Component({
  selector: 'app-topups',
  templateUrl: './topups.component.html',
  styleUrls: ['./topups.component.sass']
})
export class TopupsComponent implements OnInit {

  @Input()
  topUps?: TopUpDTO[];
  topUpStatus = TopUpStatus;

  constructor() { }

  ngOnInit(): void {
  }

}
