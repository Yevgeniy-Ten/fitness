import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { AccountDTO, AccountRole, AccountStatus, getAccountRoleNameI18nKey } from '@core/models/account.models';
import { PrincipalService } from '@core/services/principal.service';
import { AccountService } from '@core/services/account.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin-accounts-table',
  templateUrl: './admin-accounts-table.component.html',
  styleUrls: ['./admin-accounts-table.component.sass']
})
export class AdminAccountsTableComponent implements OnInit {

  @Input() accounts!: AccountDTO[];
  @Output() statusUpdated = new EventEmitter<void>();
  accountStatus = AccountStatus;
  buttonsDisabled = false;

  constructor(private principalService: PrincipalService,
              private accountService: AccountService,
              private translateService: TranslateService) { }

  ngOnInit(): void {
  }

  isMyAccount(username: string): boolean {
    const myUsername = this.principalService.getUserInfoSnapshot().username;
    return myUsername === username;
  }

  getRoleName(role: AccountRole): string {
    const i18nKey = getAccountRoleNameI18nKey(role);
    return this.translateService.instant(i18nKey);
  }

  updateAccountStatus(id: number, status: AccountStatus): void {
    this.buttonsDisabled = true;
    this.accountService.updateAccountStatus(id, status)
      .pipe(finalize(() => this.buttonsDisabled = false))
      .subscribe(_ => this.statusUpdated.emit());
  }
}
