import { Component, OnInit } from '@angular/core';
import { Page } from '@core/models/page.model';
import { TopUpDTO } from '@core/models/wallet.model';
import { WalletService } from '@core/services/wallet.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-topups-container',
  templateUrl: './topups-container.component.html',
  styleUrls: ['./topups-container.component.sass']
})
export class TopupsContainerComponent implements OnInit {

  loading = false;
  page!: Page<TopUpDTO>;
  pageSize = 10;

  constructor(private walletService: WalletService) { }

  ngOnInit(): void {
    this.getTopUps();
  }

  getTopUps(pageNumber?: number): void {
    if (this.loading === true) {
      return;
    }
    this.loading = true;
    this.walletService.getTopUps(pageNumber)
      .pipe(finalize(() => this.loading = false))
      .subscribe(page => this.page = page);
  }

  pageChanged(pageNumber: number): void {
    this.getTopUps(--pageNumber);
  }
}
