import { Component, OnInit } from '@angular/core';
import {AccountDTO} from '@core/models/account.models';
import {AccountService} from '@core/services/account.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-admin-accounts-container',
  templateUrl: './admin-accounts-container.component.html',
  styleUrls: ['./admin-accounts-container.component.sass']
})
export class AdminAccountsContainerComponent implements OnInit {

  accounts!: AccountDTO[];
  loading = false;

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.getAccounts();
  }

  getAccounts(): void {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.accountService.getAccounts()
      .pipe(finalize(() => this.loading = false))
      .subscribe(accounts => this.accounts = accounts);
  }
}
