import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TopUpDTO, TopUpStatus } from '@core/models/wallet.model';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.sass']
})
export class WalletComponent {

  @Output() payButtonClicked = new EventEmitter<number>();
  @Input()
  accountBalance!: string;
  @Input()
  topUps?: TopUpDTO[];
  topUpStatus = TopUpStatus;

  paymentAmount!: number;

  pay(): void {
    this.payButtonClicked.emit(this.paymentAmount);
  }
}
