import { Component, OnInit } from '@angular/core';
import { WalletService } from '@core/services/wallet.service';
import { finalize, map } from 'rxjs/operators';
import { TopUpDTO } from '@core/models/wallet.model';
import { AccountService } from '@core/services/account.service';
import { Page } from '@core/models/page.model';

@Component({
  selector: 'app-wallet-container',
  templateUrl: './wallet-container.component.html',
  styleUrls: ['./wallet-container.component.sass']
})
export class WalletContainerComponent implements OnInit {

  loading = false;
  accountBalance!: string;
  page!: Page<TopUpDTO>;
  pageSize = 10;

  constructor(private walletService: WalletService,
              private accountService: AccountService) {
    walletService.loadWidgetScript();
  }

  ngOnInit(): void {
    this.getAccountBalance();
    this.getTopUps(0);
  }

  openWidget(paymentAmount: number): void {
    const widget = this.walletService.loadWidget();
    this.accountService.getAccountInfo()
      .pipe(map(accountInfo => this.walletService.generatePaymentPayload(accountInfo, paymentAmount)))
      .subscribe(paymentPayload => {
        console.log(paymentPayload);
        widget.pay(
          'charge',
          paymentPayload,
          {
            onSuccess: () => {
              this.getAccountBalance();
              this.getTopUps();
            },
            onComplete: () => {
              this.walletService.saveTopUp().subscribe();
            }
          }
        );
      });
  }

  private getAccountBalance(): void {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.walletService.getAccountBalance()
      .pipe(finalize(() => this.loading = false))
      .subscribe( balance => this.accountBalance = balance.toString());
  }

  getTopUps(page?: number): void {
    this.walletService.getTopUps(page)
      .subscribe(topups => {
        this.page = topups;
      });
  }

  pageChanged(page: number): void {
    this.getTopUps(--page);
  }
}
