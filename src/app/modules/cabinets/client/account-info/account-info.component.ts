import {Component, Input} from '@angular/core';
import {AccountDTO, getAccountRoleNameI18nKey} from '@core/models/account.models';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.sass']
})
export class AccountInfoComponent {

  @Input() accountInfo!: AccountDTO;

  get accountRoleName(): string {
    return getAccountRoleNameI18nKey(this.accountInfo.role);
  }

}
