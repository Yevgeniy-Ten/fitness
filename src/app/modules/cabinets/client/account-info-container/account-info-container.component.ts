import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { AccountDTO } from '@core/models/account.models';
import { AccountService } from '@core/services/account.service';

@Component({
  selector: 'app-account-info-container',
  templateUrl: './account-info-container.component.html',
  styleUrls: ['./account-info-container.component.sass']
})
export class AccountInfoContainerComponent implements OnInit {

  loading = false;
  accountInfo!: AccountDTO;

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.getAccountInfo();
  }

  private getAccountInfo(): void {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.accountService.getAccountInfo()
      .pipe(finalize(() => this.loading = false))
      .subscribe(info => this.accountInfo = info);
  }
}
