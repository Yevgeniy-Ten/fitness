import {Component, EventEmitter, Output, OnInit, Input} from '@angular/core';
import {OrganizationService} from '@core/services/organization.service';
import {OrganizationDTO} from '@core/models/organization.model';
import {AppointmentRequestDTO} from '@core/models/appointment.model';
import {OfferedServiceService} from '@core/services/offered-service.service';
import {OfferedServiceDTO} from '@core/models/offered-service.model';
import {AppointmentService} from '@core/services/appointment.service';

@Component({
  selector: 'app-request-appointment',
  templateUrl: './request-appointment.component.html',
  styleUrls: ['./request-appointment.component.sass']
})
export class RequestAppointmentComponent implements OnInit {

  @Output() sent = new EventEmitter<void>();
  @Output() cancelled = new EventEmitter<void>();
  organizations: OrganizationDTO[] | undefined;
  offeredServices: OfferedServiceDTO[] | undefined;
  @Input() request = {} as AppointmentRequestDTO;

  constructor(private appointmentService: AppointmentService,
              private offeredServiceService: OfferedServiceService,
              private organizationService: OrganizationService) { }

  ngOnInit(): void {
    this.getOrganizations();
    this.getOfferedServices();
    console.log(this.request);
  }

  get organizationName(): string | undefined {
    return this.organizations?.find(o => o.id === this.request.organizationId)?.name;
  }

  private getOfferedServices(): void {
    this.offeredServiceService.getOfferedServices()
      .subscribe(list => {
        this.offeredServices = list;
        if (list.length === 1) {
          this.request.offeredServiceId = list[0].id;
        }
      });
  }

  private getOrganizations(): void {
    this.organizationService.getOrganizations()
      .subscribe(list => this.organizations = list);
  }

  send(): void {
    this.appointmentService.createAppointmentRequest(this.request)
      .subscribe(_ => this.sent.emit());
  }

  cancel(): void {
    this.cancelled.emit();
  }
}
