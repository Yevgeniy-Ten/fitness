import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { AccountInfoContainerComponent } from '@modules/cabinets/client/account-info-container/account-info-container.component';
import { CalendarModule } from '@modules/calendar/calendar.module';
import { ScheduleModule } from '@modules/schedule/schedule.module';
import { CoreModule } from '@core/core.module';
import { AccountInfoComponent } from './account-info/account-info.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RequestAppointmentComponent } from './request-appointment/request-appointment.component';
import { WalletContainerComponent } from './wallet-container/wallet-container.component';
import { WalletComponent } from './wallet/wallet.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    ClientComponent,
    AccountInfoContainerComponent,
    AccountInfoComponent,
    ScheduleComponent,
    RequestAppointmentComponent,
    WalletContainerComponent,
    WalletComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    FormsModule,
    CalendarModule,
    ScheduleModule,
    CoreModule,
    NgxMaskModule.forRoot(),
    TranslateModule
  ]
})
export class ClientModule { }
