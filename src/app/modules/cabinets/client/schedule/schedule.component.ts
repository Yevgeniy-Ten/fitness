import {Component, TemplateRef, ViewChild} from '@angular/core';
import { ScheduleFilterParams } from '@modules/schedule/model/schedule-filter-params';
import { ScheduleService } from '@modules/schedule/schedule.service';
import { finalize } from 'rxjs/operators';
import {DaySchedule, SelectedTimeslot} from '@modules/schedule/model/day-schedule';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {RequestAppointmentComponent} from '@modules/cabinets/client/request-appointment/request-appointment.component';
import {AppointmentRequestDTO} from '@core/models/appointment.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.sass']
})
export class ScheduleComponent {

  loading = false;
  scheduleInfo: DaySchedule | undefined;
  modalRef: BsModalRef | undefined;
  @ViewChild('requestAppointmentTemplate') requestAppointmentTemplate!: TemplateRef<RequestAppointmentComponent>;
  request = {} as AppointmentRequestDTO;
  scheduleParams: ScheduleFilterParams | undefined;

  constructor(private modalService: BsModalService,
              private scheduleService: ScheduleService,
              private toastr: ToastrService,
              private translateService: TranslateService) { }

  onFilterDataChanged(params: ScheduleFilterParams): void {
    this.loading = true;
    this.scheduleParams = params;
    this.scheduleService.getSchedule(params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => this.scheduleInfo = res);
  }

  onSlotClicked(data: SelectedTimeslot): void {
    console.log(data);
    this.request = {
      hh: Math.floor(data.timeslot / 60),
      mm: data.timeslot % 60,
      date: this.scheduleParams?.selectedDate,
      organizationId: data.employee.organizationId,
      offeredServiceId: this.scheduleParams?.offeredServiceId,
      employeeName: data.employee.fullname,
      employeeId: data.employee.ID,
      comment: ''
    } as AppointmentRequestDTO;
    this.modalRef = this.modalService.show(this.requestAppointmentTemplate, { class: 'modal-lg' });
  }

  onRequestSent(): void {
    this.closeModal();
    this.toastr.info(this.translateService.instant('client.schedule.request-sent'));
  }

  closeModal(): void {
    this.modalRef?.hide();
  }
}
