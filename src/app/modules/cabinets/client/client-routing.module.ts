import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './client.component';
import { AccountInfoContainerComponent } from '@modules/cabinets/client/account-info-container/account-info-container.component';
import { CalendarPageComponent } from '@modules/calendar/calendar-page/calendar-page.component';
import { ScheduleComponent } from '@modules/cabinets/client/schedule/schedule.component';
import { WalletContainerComponent } from '@modules/cabinets/client/wallet-container/wallet-container.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: 'account', component: AccountInfoContainerComponent },
      { path: 'calendar', component: CalendarPageComponent },
      { path: 'schedule', component: ScheduleComponent },
      { path: 'wallet', component: WalletContainerComponent },
      { path: '', redirectTo: 'calendar' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
