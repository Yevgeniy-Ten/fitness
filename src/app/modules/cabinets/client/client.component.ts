import { Component } from '@angular/core';
import {AccountRole, getAccountRoleNameI18nKey} from '@core/models/account.models';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.sass']
})
export class ClientComponent {

  accountRoleName = getAccountRoleNameI18nKey(AccountRole.ROLE_CLIENT);

  routes = [
    { url: '/client/account', name: 'routes.client.account', icon: 'accounts' },
    { url: '/client/calendar', name: 'routes.client.calendar', icon: 'calendar' },
    { url: '/client/schedule', name: 'routes.client.schedule', icon: 'calendar' },
    { url: '/client/wallet', name: 'routes.client.wallet', icon: 'wallet'}
  ];

}
