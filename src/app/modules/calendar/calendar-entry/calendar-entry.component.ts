import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {AppointmentDTO} from '@core/models/appointment.model';
import {OfferedServiceType} from '@core/models/offered-service.model';

@Component({
  selector: 'app-calendar-entry',
  templateUrl: './calendar-entry.component.html',
  styleUrls: ['./calendar-entry.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarEntryComponent {

  @Input() entry!: AppointmentDTO;
  ServiceType = OfferedServiceType;

}
