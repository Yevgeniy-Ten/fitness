import * as moment from 'moment';

export function getWeek(date: moment.Moment): moment.Moment[] {
  const day = moment(date).subtract({ days: date.isoWeekday() });

  const result = [];

  for (let i = 0; i < 7; i++) {
    day.add(1, 'days');
    result.push(moment(day.toDate()));
  }

  return result;
}

export function getMonth(date: moment.Moment): moment.Moment[] {
  const day = moment(date).subtract({ days: date.clone().date() + date.clone().startOf('month').isoWeekday() - 1 });

  const result = [];

  for (let i = 0; i < 42; i++) {
    day.add(1, 'days');
    result.push(moment(day.toDate()));
  }

  return result;
}

export function getFirstDateOfSelectedMonth(date: moment.Moment): moment.Moment {
  return moment(date).subtract({ days: date.clone().date() + date.clone().startOf('month').isoWeekday() });
}

export function getLastDateOfSelectedMonth(date: moment.Moment): moment.Moment {
  const day = getFirstDateOfSelectedMonth(date);
  return day.add(42, 'days');
}
