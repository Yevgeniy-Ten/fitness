import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import * as moment from 'moment';
import {APPOINTMENT_DATE_FORMAT, AppointmentDTO} from '@core/models/appointment.model';
import {AppointmentService} from '@core/services/appointment.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.component.html',
  styleUrls: ['./calendar-day.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarDayComponent {

  private _day!: moment.Moment;
  entries$ = new Subject<AppointmentDTO[]>();

  constructor(private appointmentService: AppointmentService) {}

  @Input()
  set day(v: moment.Moment) {
    this._day = v;
    this.entries$.next([]);
    const fromDate = this._day.format(APPOINTMENT_DATE_FORMAT);
    const tillDate = this._day.format(APPOINTMENT_DATE_FORMAT);
    this.appointmentService.getAppointments(fromDate, tillDate)
      .subscribe(entries => this.entries$.next(entries));
  }

  get date(): string | undefined {
    return this._day?.format('DD.MM');
  }

  get weekDay(): moment.Moment {
    return this._day;
  }
}
