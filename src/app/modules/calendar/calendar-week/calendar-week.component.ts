import { Component, OnInit } from '@angular/core';
import { CalendarService } from '../calendar.service';
import * as moment from 'moment';

@Component({
  selector: 'app-calendar-week',
  templateUrl: './calendar-week.component.html',
  styleUrls: ['./calendar-week.component.sass']
})
export class CalendarWeekComponent implements OnInit {

  days: moment.Moment[] | undefined;

  constructor(private service: CalendarService) { }

  ngOnInit(): void {
    this.service
      .selectedWeek
      .subscribe(days => this.days = days);
  }

}
