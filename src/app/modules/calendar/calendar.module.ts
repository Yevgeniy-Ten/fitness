import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarService } from './calendar.service';
import { CalendarPageComponent } from './calendar-page/calendar-page.component';
import { CalendarHeaderComponent } from './calendar-header/calendar-header.component';
import { CalendarWeekComponent } from './calendar-week/calendar-week.component';
import { CalendarDayComponent } from './calendar-day/calendar-day.component';
import { CalendarSideMenuComponent } from './calendar-side-menu/calendar-side-menu.component';
import { CalendarEntryComponent } from './calendar-entry/calendar-entry.component';
import { CalendarMonthComponent } from './calendar-month/calendar-month.component';
import { CoreModule } from '@core/core.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    CalendarPageComponent,
    CalendarHeaderComponent,
    CalendarWeekComponent,
    CalendarDayComponent,
    CalendarSideMenuComponent,
    CalendarEntryComponent,
    CalendarMonthComponent
  ],
  providers: [
    CalendarService
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    TranslateModule
  ]
})
export class CalendarModule { }
