import {Component, OnInit} from '@angular/core';
import { CalendarService } from '../calendar.service';
import * as moment from 'moment';
import {map} from 'rxjs/operators';

import {AppointmentService} from '@core/services/appointment.service';
import {getFirstDateOfSelectedMonth, getLastDateOfSelectedMonth, getMonth} from '@modules/calendar/calendar.utils';
import {APPOINTMENT_DATE_FORMAT} from '@core/models/appointment.model';
import {OfferedServiceType} from '@core/models/offered-service.model';

@Component({
  selector: 'app-calendar-month',
  templateUrl: './calendar-month.component.html',
  styleUrls: ['./calendar-month.component.sass'],
})
export class CalendarMonthComponent implements OnInit {

  day!: moment.Moment;
  days: moment.Moment[] | undefined;
  activeDate: string | undefined;
  activities: Map<string, string[]> | undefined;
  serviceType = OfferedServiceType;

  constructor(private calendarService: CalendarService,
              private appointmentService: AppointmentService) {}

  ngOnInit(): void {
    this.calendarService.selectedDay
      .subscribe(
      day => {
        this.day = day;
        this.activeDate = day.format('DD, MM, yyyy');
        this.days = getMonth(day);
        this.getMonthActivities(day);
      }
    );
  }

  prevMonth(): void {
    this.calendarService.prevMonth();
  }

  nextMonth(): void {
    this.calendarService.nextMonth();
  }

  getDate(date: moment.Moment): string {
    return date.format('DD');
  }

  private getMonthActivities(date: moment.Moment): void {
    const fromDate = getFirstDateOfSelectedMonth(date).format(APPOINTMENT_DATE_FORMAT);
    const tillDate = getLastDateOfSelectedMonth(date).format(APPOINTMENT_DATE_FORMAT);
    this.appointmentService.getAppointments(fromDate, tillDate)
      .pipe(map(appointments => {
        const activitiesMap = new Map();
        appointments.forEach(app => {
          const activity = activitiesMap.get(app.date);
          if (!!activity) {
            activity.push(app.type);
          } else {
            activitiesMap.set(app.date, [app.type]);
          }
        });
        return activitiesMap;
      }))
      .subscribe(activities => {
        this.activities = activities;
      });
  }

  selectDate(date: moment.Moment): void {
    this.calendarService.changeSelectedDate(date);
  }

  // showCircle(day: string, service: OfferedServiceType): boolean {
  //   const dayActivities = this.activities?.get(day);
  //   if (dayActivities) {
  //     return dayActivities.includes(service);
  //   }
  //   return false;
  // }

  hasActivity(day: string): boolean {
    return !!this.activities?.get(day);
  }
}
