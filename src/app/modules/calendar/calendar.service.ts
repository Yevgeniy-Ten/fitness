import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import { getWeek } from './calendar.utils';

@Injectable()
export class CalendarService {

  readonly selectedDay: BehaviorSubject<moment.Moment>;

  constructor() {
    this.selectedDay = new BehaviorSubject<moment.Moment>(moment(new Date()));
  }

  get selectedWeek(): Observable<moment.Moment[]> {
    return this.selectedDay
      .pipe(map(day => getWeek(day)));
  }

  prev(): void {
    const day = this.selectedDay.value;
    this.selectedDay.next(day.subtract({ days: 7 }));
  }

  next(): void {
    const day = this.selectedDay.value;
    this.selectedDay.next(day.add({ days: 7 }));
  }

  prevMonth(): void {
    const day = this.selectedDay.value;
    this.selectedDay.next(day.subtract({ month: 1 }).startOf('month'));
  }

  nextMonth(): void {
    const day = this.selectedDay.value;
    this.selectedDay.next(day.add({ month: 1 }).startOf('month'));
  }

  today(): void {
    this.selectedDay.next(moment(new Date()));
  }

  changeSelectedDate(date: moment.Moment): void {
    this.selectedDay.next(date);
  }
}
