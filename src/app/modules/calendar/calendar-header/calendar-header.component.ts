import { Component, OnInit } from '@angular/core';
import {CalendarService} from '../calendar.service';
import * as moment from 'moment';

@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.sass']
})
export class CalendarHeaderComponent implements OnInit {

  day!: moment.Moment;

  constructor(private service: CalendarService) { }

  ngOnInit(): void {
    this.service.selectedDay.subscribe(day => this.day = day);
  }

  prev(): void {
    this.service.prev();
  }

  next(): void {
    this.service.next();
  }

  today(): void {
    this.service.today();
  }
}
