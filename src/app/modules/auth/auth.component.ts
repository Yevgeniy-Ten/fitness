import { Component, OnInit } from '@angular/core';
import {PrincipalService} from '../core/services/principal.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit {

  constructor(private principal: PrincipalService,
              private router: Router) { }

  ngOnInit(): void {
    if (this.principal.isAuthenticated()) {
      this.router.navigate(['cabinet']);
    }
  }

}
