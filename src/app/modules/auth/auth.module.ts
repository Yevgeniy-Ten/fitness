import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { HeaderComponent } from './header/header.component';
import { AuthComponent } from './auth.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { CoreModule } from '../core/core.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    HeaderComponent,
    AuthComponent,
    RestorePasswordComponent
  ],
    imports: [
        HttpClientModule,
        CommonModule,
        AuthRoutingModule,
        ReactiveFormsModule,
        CoreModule,
        NgxMaskModule.forRoot(),
        TranslateModule
    ]
})
export class AuthModule { }
