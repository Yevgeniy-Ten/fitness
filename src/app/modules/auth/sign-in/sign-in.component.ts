import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { AccountRole } from '../../core/models/account.models';
import { IAuthResult } from '../../core/models/auth.models';
import { AuthApiService } from '../../core/services/auth-api.service';
import { PrincipalService } from '../../core/services/principal.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.sass']
})
export class SignInComponent implements OnInit {
  form!: FormGroup;
  errorMessageI18nKey!: string;
  loading = false;

  constructor(private authService: AuthApiService,
              private principalService: PrincipalService,
              private router: Router) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  signin(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.errorMessageI18nKey = 'auth.sign-in.invalid-form';
      return;
    }
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.form.disable();
    this.authService.signin(this.form.value)
      .pipe(finalize(() => {
        this.form.enable();
        this.loading = false;
      }))
      .subscribe(
      user => {
        this.principalService.setUser(user);
        this.navigateToCabinet(user);
      },
      error => {
        if (error.status === 400) {
          this.errorMessageI18nKey = 'auth.sign-in.invalid-password';
        }
        else if (error.status === 500) {
          this.errorMessageI18nKey = 'auth.sign-in.invalid-account';
        }
        else {
          this.errorMessageI18nKey = 'auth.sign-in.service-unavailable';
        }
      });
  }

  private navigateToCabinet(user: IAuthResult): void {
    switch (user.role) {
      case AccountRole.ROLE_ADMIN:
        this.router.navigate(['admin']);
        return;
      default:
        this.router.navigate(['client']);
    }
  }
}
