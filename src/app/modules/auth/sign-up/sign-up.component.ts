import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { AuthApiService } from '../../core/services/auth-api.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.sass']
})

export class SignUpComponent implements OnInit {
  form!: FormGroup;
  isSignedUp = false;
  statusMessage!: string;
  loading = false;

  constructor(private authApiService: AuthApiService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      fullname: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required)
    });
  }

  signup(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.statusMessage = 'auth.sign-up.invalid-form';
      return;
    }
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.form.disable();
    this.authApiService.signup(this.form.value)
      .pipe(finalize(() => {
        this.loading = false;
        this.form.enable();
      }))
      .subscribe(
      () => {
          this.isSignedUp = true;
          this.statusMessage = 'auth.sign-up.signup-success';
        },
      (err) => {
          this.isSignedUp = false;
          if (err.status === 400) {
            this.statusMessage = 'auth.sign-up.account-already-exist';
          } else {
            this.statusMessage = 'auth.sign-up.signup-failure';
          }
    });
  }
}
