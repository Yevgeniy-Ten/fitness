export const environment = {
  production: true,
  auth: '/api/v1/auth',
  api: '/api/v1',
  cloudPaymentsPublicId: 'pk_9581c36948f908589db843350d503',
  cloudPaymentsDescription: 'Пополнение кошелька на new.grandprixclub.kz'
};
