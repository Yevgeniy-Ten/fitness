### STAGE 1: Build ###
FROM node:16.13.0 as build
RUN mkdir -p /app
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . ./
RUN npm run build -- --configuration=production

### STAGE 2: Run ###
FROM nginx:latest
COPY --from=build /app/dist/cabinet /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
